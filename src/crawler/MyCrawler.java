package crawler;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import dictionary.AllDocuments;
import dictionary.Dictionary;
import dictionary.Document;
import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;

public class MyCrawler extends WebCrawler {

	private int crawled = 0;
	public static Set<String> myDocuments = new HashSet();
	private final String dir = "docs";

	private final static Pattern FILTERS = Pattern
			.compile(".*(\\.(css|js|gif|jpg" + "|png|mp3|mp3|zip|gz))$");

	/**
	 * This method receives two parameters. The first parameter is the page in
	 * which we have discovered this new url and the second parameter is the new
	 * url. You should implement this function to specify whether the given url
	 * should be crawled or not (based on your crawling logic). In this example,
	 * we are instructing the crawler to ignore urls that have css, js, git, ...
	 * extensions and to only accept urls that start with
	 * "http://www.ics.uci.edu/". In this case, we didn't need the referringPage
	 * parameter to make the decision.
	 */
	@Override
	public boolean shouldVisit(Page referringPage, WebURL url) {
		String href = url.getURL().toLowerCase();
		return !FILTERS.matcher(href).matches()
				&& href.startsWith("http://en.wikipedia.org/wiki");
	}

	/**
	 * This function is called when a page is fetched and ready to be processed
	 * by your program.
	 */
	@Override
     public void visit(Page page) {
         String url = page.getWebURL().getURL();

         if (page.getParseData() instanceof HtmlParseData) {
             HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();
             String text = htmlParseData.getText();
             String wiki = " - Wikipedia, the free encyclopedia";
             String title = htmlParseData.getTitle().substring(0, htmlParseData.getTitle().length()-wiki.length());
             
             try(BufferedWriter bw = new BufferedWriter(new FileWriter(dir+"/"+title))){
            	 bw.write(text);
             } catch (Exception e) {
				e.printStackTrace();
			}
             
             /*List<String> words = Dictionary.get().addWords(text);
             Document document = new Document(htmlParseData.getTitle());
             document.setWords(words);
             AllDocuments.get().addDocument(document);*/

             System.out.println(Controller.getAmount());
         }
    }
}