package crawler;

import dictionary.AllDocuments;
import dictionary.Dictionary;
import dictionary.Document;
import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;

public class Controller {

	private static int crawled = 0;

	public static synchronized int getAmount() {
		return crawled++;
	}

	public static void main(String[] args) throws Exception {
		String crawlStorageFolder = "crawl/root";
		int numberOfCrawlers = 7;
		int maxPagesToFetch = 10000;

		CrawlConfig config = new CrawlConfig();
		config.setCrawlStorageFolder(crawlStorageFolder);
		config.setMaxPagesToFetch(maxPagesToFetch);

		/*
		 * Instantiate the controller for this crawl.
		 */
		PageFetcher pageFetcher = new PageFetcher(config);
		RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
		RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig,
				pageFetcher);
		CrawlController controller = new CrawlController(config, pageFetcher,
				robotstxtServer);

		/*
		 * For each crawl, you need to add some seed urls. These are the first
		 * URLs that are fetched and then the crawler starts following links
		 * which are found in these pages
		 */
		controller.addSeed("http://en.wikipedia.org/wiki/Palazzo_Comunale,_San_Gimignano");

		/*
		 * Start the crawl. This is a blocking operation, meaning that your code
		 * will reach the line after this only when crawling is finished.
		 */

		controller.start(MyCrawler.class, numberOfCrawlers);

		//Dictionary.get().print();
		//AllDocuments.get().getDocuments().stream().forEach(Document::determineVector);
		//AllDocuments.get().getDocuments().stream().forEach(Document::printVector);
		

	}
}