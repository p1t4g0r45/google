package dictionary;

import java.util.HashSet;
import java.util.Set;

public class AllDocuments {
	private static AllDocuments instance = null;
	private final Set<Document> documentSet;

	private AllDocuments() {
		documentSet = new HashSet<Document>();
	}

	public synchronized static AllDocuments get() {
		if (instance == null) {
			instance = new AllDocuments();
		}
		return instance;
	}
	
	public synchronized void addDocument(Document document){
		documentSet.add(document);
	}
	
	public Set<Document> getDocuments(){
		return documentSet;
	}
}
