package dictionary;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class IDFMultiplicator {

	private final String dir = "mappedDocs";
	private final String dictionary = "dictionary";
	private final Map<String, Integer> wordsAsMap;
	private final Map<Integer, String> invertedWordsAsMap;
	private final Map<String, Integer> wordsInAllDocumentsCounter;
	private Map<String, Double> IDF;

	public static void main(String[] args) {

		new IDFMultiplicator();

	}

	public IDFMultiplicator() {
		Dictionary.get().createDictionaryFromFile(new File(dictionary));
		wordsAsMap = Dictionary.get().getWordsAsMap();
		invertedWordsAsMap = Dictionary.get().getInverseMap();
		wordsInAllDocumentsCounter = new HashMap();

		countWords();
		IDF = createIDF();
		multiplyByIDF();

		for (int i = 0; i < 100; i++) {
			System.out.println(invertedWordsAsMap.get(i) + "="
					+ wordsInAllDocumentsCounter.get(invertedWordsAsMap.get(i))
					+ " | " + IDF.get(invertedWordsAsMap.get(i)));
		}

		// matrix.showGUI();
	}

	private void countWords() {
		File directory = new File(dir);

		int i = 0;
		for (File doc : directory.listFiles()) {
			Map<Integer, Double> text = readFile(doc);

			final int row = i;
			text.keySet()
					.stream()
					.forEach(
							k -> wordsInAllDocumentsCounter.put(
									invertedWordsAsMap.get(k),
									wordsInAllDocumentsCounter
											.get(invertedWordsAsMap.get(k)) == null ? 1
											: (wordsInAllDocumentsCounter
													.get(invertedWordsAsMap
															.get(k)) + 1)));
			i++;
			if (i % 100 == 0)
				System.out.println(i);
		}
	}

	private Map<String, Double> createIDF() {
		Map<String, Double> IDF = new HashMap();
		for (Iterator iterator = wordsInAllDocumentsCounter.keySet().iterator(); iterator
				.hasNext();) {
			String s = (String) iterator.next();
			IDF.put(s,
					Math.log((double) wordsAsMap.size()
							/ wordsInAllDocumentsCounter.get(s)));
		}
		return IDF;
	}

	private void multiplyByIDF() {
		File directory = new File(dir);

		int i = 0;
		for (File doc : directory.listFiles()) {
			Map<Integer, Double> text = readFile(doc);
			text.keySet()
					.stream()
					.forEach(
							k -> text.put(
									k,
									text.get(k)
											* IDF.get(invertedWordsAsMap.get(k))));

			writeMapToFile(doc, text);
			i++;
			if (i % 100 == 0)
				System.out.println(i);
		}
	}

	private void writeMapToFile(File doc, Map<Integer, Double> text) {
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(doc))) {
			for (int k : text.keySet()) {
				bw.write(k + "=" + text.get(k).intValue() + "\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Map<Integer, Double> readFile(File doc) {
		Map<Integer, Double> map = new HashMap();
		try (BufferedReader br = new BufferedReader(new FileReader(doc))) {
			String line = null;
			while ((line = br.readLine()) != null) {
				String[] split = line.split("=");
				map.put(Integer.parseInt(split[0]),
						Double.parseDouble(split[1]));
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return map;
	}

}
