package dictionary;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class TextToMapParser {

	private final String dir = "docs";
	static final String mappedDir = "mappedDocs";

	public static void main(String[] args) {
		new TextToMapParser();
	}

	public TextToMapParser() {
		File directory = new File(dir);

		int i = 0;
		for (File doc : directory.listFiles()) {
			String text = readFile(doc);

			List<String> words = Dictionary.get().addWords(text);
			Document document = new Document(doc.getName());
			document.setWords(words);
			AllDocuments.get().addDocument(document);

			i++;
			if (i % 100 == 0)
				System.out.println(i);

		}
		
		System.out.println(Dictionary.get().getWordsAsList().size());
		Dictionary.get().deleteSingleOccurances();
		
		AllDocuments.get().getDocuments().stream().forEach(Document::determineVector);
		Dictionary.get().writeDictionaryToFile();
		
		
	}

	private String readFile(File doc) {
		String content = null;
		try {
			content = new Scanner(doc).useDelimiter("\\Z")
					.next();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return content;
	}

}
