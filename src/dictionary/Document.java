package dictionary;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Document {

	private final String title;
	private Map<String, Integer> occurance;
	private Map<Integer, Integer> bagOfWords;

	private final String filename = "tmp";

	public Document(String title) {
		this.title = title;
		occurance = new HashMap();
	}

	public void setWords(List<String> subset) {
		subset.stream().forEach(
				s -> occurance.put(s,
						occurance.containsKey(s) ? occurance.get(s) + 1 : 1));
	}

	public void determineVector() {
		Set<String> wordsAsSet = Dictionary.get().getWordsAsSet();
		Map<String, Integer> wordsAsMap = Dictionary.get().getWordsAsMap();
		bagOfWords = new HashMap();
		occurance.keySet().stream().filter(k -> wordsAsSet.contains(k))
				.forEach(k -> bagOfWords.put(wordsAsMap.get(k), occurance.get(k)));
		writeBagOfWordsToFile();
	}

	public void print() {
		occurance.entrySet().stream().forEach(System.out::println);
	}

	public Map<String, Integer> getOccurance() {
		return occurance;
	}

	public void writeBagOfWordsToFile() {
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(
				TextToMapParser.mappedDir + "/" + title))) {
			for(int i : bagOfWords.keySet()){
				bw.write(i + "=" + bagOfWords.get(i) + "\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
