package dictionary;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class Dictionary {

	private final String filename = "stop_words.txt";
	private Set<String> words;
	private Map<String, Integer> wordsAsMap;
	private static Dictionary instance = null;
	private final Set<String> stopWords;
	private final String dictionary = "dictionary";

	private Dictionary() {
		words = new TreeSet<String>();
		stopWords = new HashSet<String>();
		inectStopWordsFromFile();
	}

	private void inectStopWordsFromFile() {
		try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
			String line = null;
			while ((line = br.readLine()) != null) {
				stopWords.add(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Dictionary get() {
		if (instance == null) {
			instance = new Dictionary();
		}
		return instance;
	}

	public synchronized List<String> addWords(String text) {
		List<String> subset = Arrays
				.asList(text.replaceAll("\n", " ").split(" "))
				.stream()
				.map(s -> s.replaceAll("[^a-zA-Z]", "").toLowerCase())
				.filter(s -> (s.length() > 1 && s.length() < 13 && !stopWords
						.contains(s))).collect(Collectors.toList());
		// subset.stream().filter(s -> !words.contains(s)).forEach(words::add);
		words.addAll(subset);
		return subset;
	}

	public void deleteSingleOccurances() {
		Map<String, Integer> occurance = new HashMap();
		words.stream().forEach(w -> occurance.put(w, 0));
		AllDocuments
				.get()
				.getDocuments()
				.stream()
				.forEach(
						d -> d.getOccurance()
								.keySet()
								.stream()
								.forEach(
										k -> occurance.put(k, occurance.get(k)
												+ d.getOccurance().get(k))));
		System.out.println(occurance.keySet().stream()
				.filter(k -> occurance.get(k) > 1).count());
		words = occurance.keySet().stream().filter(k -> occurance.get(k) > 1)
				.collect(Collectors.toSet());
		wordsAsMap = new HashMap();
		int i = 0;
		for (String s : words) {
			wordsAsMap.put(s, i);
			i++;
		}
	}

	public List<String> getWordsAsList() {
		return words.stream().collect(Collectors.toList());
	}

	public Set<String> getWordsAsSet() {
		return words;
	}

	public Map<String, Integer> getWordsAsMap() {
		return wordsAsMap;
	}

	public void writeDictionaryToFile() {
		List<String> myWords = getWordsAsList();
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(dictionary))) {
			for (int i = 0; i < words.size(); i++) {
				bw.write(i + "=" + myWords.get(i) + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void createDictionaryFromFile(File f) {
		if (wordsAsMap != null)
			throw new IllegalStateException(
					"Method available only when creating dictionary");
		wordsAsMap = new HashMap();
		try (BufferedReader br = new BufferedReader(new FileReader(f))) {
			String line = null;
			while ((line = br.readLine()) != null) {
				String[] split = line.split("=");
				wordsAsMap.put(split[1], Integer.parseInt(split[0]));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Map<Integer, String> getInverseMap(){
		Map<Integer, String> newMap = new HashMap();
		wordsAsMap.keySet().stream().forEach(k -> newMap.put(wordsAsMap.get(k), k));
		return newMap;
	}

}
