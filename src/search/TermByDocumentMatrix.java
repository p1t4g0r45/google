package search;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.ujmp.core.Matrix;
import org.ujmp.core.calculation.Calculation;
import org.ujmp.core.matrix.SparseMatrix;
import org.ujmp.core.objectmatrix.impl.DefaultSparseColumnObjectMatrix2D;

import dictionary.Dictionary;
import dictionary.IDFMultiplicator;

@Deprecated
public class TermByDocumentMatrix {

	private SparseMatrix matrix;
	private final String dir = "mappedDocs";
	private final String dictionary = "dictionary";
	private final Map<String, Integer> wordsAsMap;
	private final Map<Integer, String> invertedWordsAsMap;
	private Map<Integer, String> docNames;
	File directory = new File(dir);

	public static void main(String[] args) {
		new TermByDocumentMatrix();
	}

	public TermByDocumentMatrix() {
		Dictionary.get().createDictionaryFromFile(new File(dictionary));
		wordsAsMap = Dictionary.get().getWordsAsMap();
		invertedWordsAsMap = Dictionary.get().getInverseMap();
		docNames = new HashMap();
		createMatrix();

		// 74900=terrorism
		// 74784=osama
		// 268934=laden

		SparseMatrix q = SparseMatrix.factory.zeros(1, wordsAsMap.size());

		q.setAsInt(1, 0, 74900);
		q.setAsInt(1, 0, 74784);
		q.setAsInt(1, 0, 268934);
		int length = 3;
		int arr[] = new int[length];
		arr[0] = 74900;
		arr[1] = 74784;
		arr[2] = 268934;

		double q_norm = q.normF();
		int documentCount = directory.listFiles().length;
		double max = 0;
		int maxi = 0;
		for (int i = 0; i < documentCount; i++) {

			Matrix d = matrix.selectColumns(Calculation.LINK, i);
			double d_norm = d.normF();
			int sum = 0;
			for (int j = 0; j < length; j++) {
				sum += d.getAsInt(arr[j], 0);
			}
			double cos = sum / (q_norm * d_norm);
			if (cos > max) {
				max = cos;
				maxi = i;
			}

			if (i % 100 == 0)
				System.out.println(i);
		}
		System.out.println(docNames.get(maxi));
		// q.showGUI();

	}

	private void createMatrix() {
		matrix = SparseMatrix.factory.zeros(wordsAsMap.size(),
				directory.listFiles().length);

		int i = 0;
		for (File doc : directory.listFiles()) {
			Map<Integer, Double> text = IDFMultiplicator.readFile(doc);

			final int column = i;
			text.keySet()
					.stream()
					.forEach(
							k -> matrix.setAsInt(text.get(k).intValue(), k,
									column));

			docNames.put(i, doc.getName());
			i++;
			if (i % 100 == 0)
				System.out.println(i);

		}
	}
}
