package search;

import java.io.File;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.la4j.Matrix;
import org.la4j.decomposition.SingularValueDecompositor;
import org.la4j.matrix.sparse.CCSMatrix;
import org.la4j.vector.sparse.CompressedVector;

import GUI.SearchPanel;
import dictionary.Dictionary;
import dictionary.IDFMultiplicator;

public class La4JMatrix {

	private CCSMatrix matrix;
	private final String dir = "mappedDocs";
	private final String dictionary = "dictionary";
	private final Map<String, Integer> wordsAsMap;
	private final Map<Integer, String> invertedWordsAsMap;
	private Map<Integer, String> docNames;
	File directory = new File(dir);
	
	int wordsLen = 2000;
	int docs = 100;

	private class Result implements Comparable<Result> {
		int id;
		double val;

		public Result(int id) {
			this.id = id;
		}

		@Override
		public int compareTo(Result o) {
			return Integer.valueOf(id).compareTo(Integer.valueOf(o.id));
		}
	}

	public La4JMatrix() {
		Dictionary.get().createDictionaryFromFile(new File(dictionary));
		wordsAsMap = Dictionary.get().getWordsAsMap();
		invertedWordsAsMap = Dictionary.get().getInverseMap();
		docNames = new HashMap();

		//comment this
		wordsLen = wordsAsMap.size();
		docs = directory.listFiles().length;

		createMatrix();
		
		//SVD();
	}

	private void createMatrix() {
		matrix = new CCSMatrix(wordsLen, docs);

		int i = 0;
		for (File doc : directory.listFiles()) {
			Map<Integer, Double> text = IDFMultiplicator.readFile(doc);

			final int column = i;
			text.keySet()
					.stream()
					.filter(k -> k<wordsLen)
					.forEach(k -> matrix.set(k, column, text.get(k).intValue()));

			docNames.put(i, doc.getName());
			i++;
			if(i==docs) break;
			if (i % 100 == 0)
				System.out.println(i);

		}
	}

	public List<String> getResults(String[] words) {
		List<Integer> list = Arrays.asList(words).stream()
				.map(w -> wordsAsMap.get(w.toLowerCase()))
				.filter(w -> w != null)
				.filter(w -> w < wordsLen).collect(Collectors.toList());

		int length = list.size();

		double q_norm = Math.sqrt(length);
		int documentCount = docs;
		Set<Result> res = new TreeSet();
		for (int i = 0; i < SearchPanel.N; i++) {
			res.add(new Result(i));
		}
		double minRes = 0;
		for (int i = 0; i < documentCount; i++) {

			CompressedVector d = (CompressedVector) matrix.getColumn(i);
			double d_norm = d.norm();
			int sum = 0;
			for (int j = 0; j < length; j++) {
				sum += d.get(list.get(j));
			}
			double cos = sum / (q_norm * d_norm);
			if (cos > minRes) {
				Result smallest = null;
				for (Result r : res) {
					if (r.val == minRes) {
						smallest = r;
						break;
					}
				}
				smallest.id = i;
				smallest.val = cos;
				minRes = cos;
				for (Result r : res) {
					if (r.val < minRes) {
						minRes = r.val;
					}
				}
			}
		}
		List<Result> resAsList = res.stream().collect(Collectors.toList());
		resAsList.sort(new Comparator<Result>() {
			@Override
			public int compare(Result o1, Result o2) {
				return Double.valueOf(o2.val).compareTo(Double.valueOf(o1.val));
			}
		});

		return resAsList.stream().map(r -> docNames.get(r.id))
				.collect(Collectors.toList());

	}

	private void SVD(){
		
		SingularValueDecompositor decompositor = new SingularValueDecompositor(
				matrix);
		Matrix[] svd =  decompositor.decompose();
		int k = 10;
		svd[0] = svd[0].copyOfShape(svd[0].rows(), k);
		svd[1] = svd[1].copyOfShape(k, k);
		svd[2] = svd[2].copyOfShape(k, svd[2].rows());
		
		Matrix m = svd[0].multiply(svd[1]).multiply(svd[2]);
		matrix = CCSMatrix.fromCSV(m.toCSV());
	}
}
