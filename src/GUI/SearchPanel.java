package GUI;

import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import search.La4JMatrix;

public class SearchPanel extends JPanel implements ActionListener {
	public static final int N = 10;
	private final JTextField inputField;
	private final JLabel captionLabel;
	private final JButton[] results;
	private final JPanel resultsContainer;
	
	private final La4JMatrix solver;

	public SearchPanel() {
		super();

		solver = new La4JMatrix();
		inputField = new JTextField(50);
		inputField.setActionCommand("search");
		inputField.addActionListener(this);
		inputField.requestFocus();
		captionLabel = new JLabel("Type in words:");
		captionLabel.setLabelFor(inputField);

		JPanel textControlsPane = new JPanel();
		GridBagLayout gridbag = new GridBagLayout();

		textControlsPane.setLayout(gridbag);
		textControlsPane.add(captionLabel);
		textControlsPane.add(inputField);

		results = new JButton[N];
		resultsContainer = new JPanel();
		resultsContainer.setPreferredSize(new Dimension(250, 28*N));
		for (int i = 0; i < results.length; i++) {
			results[i] = new JButton();
			results[i].hide();
			results[i].addActionListener(this);
			results[i].setActionCommand(String.valueOf(i));
			resultsContainer.add(results[i]);
		}

		resultsContainer.setLayout(new BoxLayout(resultsContainer, BoxLayout.Y_AXIS));
		add(textControlsPane);
		add(resultsContainer);
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		if (event.getActionCommand().equals("search")) {
			for (int i = 0; i < results.length; i++) {
				results[i].setText("");
				results[i].show();
			}
			String text = (((JTextField) event.getSource()).getText());
			List<String> results2 = solver.getResults(text.split(" "));
			setResults(results2);
		} else {
			int x = Integer.parseInt(event.getActionCommand());
			try {
				java.awt.Desktop.getDesktop().open(new File("docs/"+results[x].getText()));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void setResults(List<String> res){
		int i = 0;
		if(res.size() > N) {
			res = res.subList(0, N);
		}
		res.stream().forEach(System.out::println);
		for (Iterator iterator = res.iterator(); iterator.hasNext();) {
			String string = (String) iterator.next();
			results[i++].setText(string);
		}
	}

}
