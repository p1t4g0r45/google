package GUI;

import java.awt.BorderLayout;

import javax.swing.JFrame;

public class SearchWindow {

	private static void createAndShowGUI() {
        JFrame frame = new JFrame("Google");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        frame.setContentPane(new SearchPanel());
 
        frame.pack();
        frame.setVisible(true);
    }
 
    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }

}
